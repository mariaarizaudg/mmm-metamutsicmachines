# MMM metamutsicmachines

MMM es un proyecto de investigación sonora y escultórica, entre otras cosas. Se trata de crear un compositor sonoro no-humano, una máquina/escultura que se inspira, alimenta y "aprende" de la música creada durante siglos por la cultura humana. [http://noconventions.mobi/noish/hotglue/?MMM/](http://noconventions.mobi/noish/hotglue/?MMM/)